
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;
import io.appium.java_client.remote.MobileCapabilityType;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.MalformedURLException;
import java.net.URL;

public class AppStore {
    private String reportDirectory = "reports";
    private String reportFormat = "xml";
    private String testName = "Untitled";
    protected IOSDriver<IOSElement> driver = null;

    DesiredCapabilities dc = new DesiredCapabilities();
    String excelFilePath = "D:\\ExcelFile\\AppStore.xlsx"; //Go to 'Excel File' folder > Right click on AppStore.xlsx > Copy path > Paste here
    int howManyTimes = 2; //Put the number how many times want to run this case

    @Before
    public void setUp() throws MalformedURLException {
        dc.setCapability("reportDirectory", reportDirectory);
        dc.setCapability("reportFormat", reportFormat);
        dc.setCapability("testName", testName);
        dc.setCapability(MobileCapabilityType.UDID, "659j5"); //Put you phone's UDID here

        driver = new IOSDriver<IOSElement>(new URL("http://localhost:4723/wd/hub"), dc);
    }

    @Test
    public void appStoreReview() throws Exception {
        for (int i = 1; i <= howManyTimes; i++){
            Thread.sleep(2000);
            driver.findElement(By.xpath("//*[@text='Safari']")).click();
            String appUrl = ExcelRead.readData(excelFilePath, 0, i+1, 2);
            driver.findElement(By.xpath("//*[@accessibilityLabel='URL']")).sendKeys(appUrl);
            driver.findElement(By.xpath("//*[@accessibilityLabel='Go']")).click();
            driver.findElement(By.xpath("//*[@value='Open']")).click();
            new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@text='Reviews']")));

            By openBtn = By.xpath("//*[@text='OPEN']");
            By updateBtn = By.xpath("//*[@text='UPDATE']");

            if (driver.findElements(openBtn).size() > 0){
                ExcelWrite.writeData(excelFilePath, 0, i+1, 6, "No");
            }else{
                ExcelWrite.writeData(excelFilePath, 0, i+1, 6, "Yes");
            }

            driver.findElement(By.xpath("//*[@value='Reviews']")).click();
            new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@value='Write a Review']")));
            driver.findElement(By.xpath("//*[@value='Write a Review']")).click();
            new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@text='Rating']")));

            if(driver.findElements(By.xpath("//*[@value='Title']")).size() > 0){
                driver.findElement(By.xpath("//*[@text='Rating']")).click();

                String reviewTitle = ExcelRead.readData(excelFilePath, 0, i+1, 3);
                driver.findElement(By.xpath("//*[@value='Title']")).click();
                driver.findElement(By.xpath("//*[@value='Title']")).sendKeys(reviewTitle);

                String reviewBody = ExcelRead.readData(excelFilePath, 0, i+1, 4);
                driver.findElement(By.xpath("//*[@value='Review']")).sendKeys(reviewBody);

                driver.findElement(By.xpath("//*[@value='Send']")).click();
                String nickname = ExcelRead.readData(excelFilePath, 0, i+1, 5);
                driver.findElement(By.xpath("//*[@value='nickname']")).sendKeys(nickname);
                driver.findElement(By.xpath("//*[@value='OK']")).click();

                Thread.sleep(2000);
                driver.findElement(By.xpath("//*[@value='Cancel']")).click();
                driver.executeScript("seetest:client.deviceAction(\"Home\")");
            }else {
                String existingReview = driver.findElement(By.xpath("//*[@XCElementType='XCUIElementTypeTextField']")).getText();
                ExcelWrite.writeData(excelFilePath, 0, i+1, 7, existingReview);
                driver.executeScript("seetest:client.deviceAction(\"Home\")");
            }
        }
    }

    @After
    public void tearDown() {
        driver.quit();
    }
}