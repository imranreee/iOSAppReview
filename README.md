# Why this project?
This is iOS automation project for app reviewing

# Prerequisites to use the project
* Download JDK: http://www.oracle.com/technetwork/java/javase/downloads/jdk10-downloads-4416644.html
* Set environment variable for JDK: https://www.mkyong.com/java/how-to-set-java_home-environment-variable-on-mac-os-x/
* Download SDK: https://developer.android.com/studio/index.html
* Set environment variable for SDK:  https://stackoverflow.com/questions/19986214/setting-android-home-enviromental-variable-on-mac-os-x
* Download Appium Studio: https://experitest.com/mobile-test-automation/appium-studio/
* Add device to appium studio: https://docs.experitest.com/display/TD/AS+-+iOS+-+Build+your+first+test
* Download IntelliJ IDEA: https://www.jetbrains.com/idea/download/


# Framework and tools
* Appium(http://appium.io/)
* TestNG and JUnit
* Reporting: extent report (http://extentreports.com/)
